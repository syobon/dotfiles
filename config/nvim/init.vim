if &compatible
	set nocompatible
endif

set number
set hls
set expandtab
set smartindent
set shiftwidth=4
set softtabstop=4
set autochdir
set encoding=utf-8
set fileencodings=utf-8,cp932
set clipboard+=unnamed,unnamedplus

syntax enable
filetype plugin indent on

let g:airline_powerline_fonts = 1
autocmd FileType * RainbowParentheses

let g:coc_global_extensions = ['coc-snippets', 'coc-json', 'coc-rust-analyzer']

set background=dark
let g:one_allow_italics = 1
colorscheme one
hi Normal ctermbg = none
